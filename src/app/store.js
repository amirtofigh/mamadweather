import { configureStore } from '@reduxjs/toolkit'
import locationsSlice from "../features/Locations/locationsSlice";
import mainCitySlice from "../features/MainCity/MainCitySlice";

const store = configureStore({
    reducer: {
        Locations: locationsSlice,
        MainCity: mainCitySlice
    }
})

export default store