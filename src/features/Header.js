import Logo from "../img/Logo.png";
import {useDispatch, useSelector} from "react-redux";
import {fetchMainCityData} from "./MainCity/MainCitySlice";
// import {deleteAddedLocation, fetchAddedLocations} from "./Locations/locationsSlice";


const openLocations = () => {
    document.querySelector(".locations").className = "locations locations-open"
}

function Header() {
    const dispatch = useDispatch()
    let mainCityStatus = useSelector(state => state.MainCity.status)
    let currentCity = useSelector(state => state.MainCity.mainCityData)
    let addedCities = useSelector(state => state.Locations.addedCities)

    const handleRefreshData = () => {
        if (mainCityStatus !== 'loading') {
            dispatch(fetchMainCityData(currentCity.city))
            // addedCities.forEach(city => {
            //     dispatch(deleteAddedLocation({lat: city.lat, lon: city.lon, name: city.cityName}))
            //     dispatch(fetchAddedLocations({lat: city.lat, lon: city.lon, name: city.cityName}))
            // })
        }
    }

    return (
        <>
            <section className="header">
                <a href="/" className="logo">
                    <img src={Logo} alt="Logo"/>
                </a>
                <div className="cities-theme">
                    <span className="reload" onClick={handleRefreshData}><i className="fa-solid fa-refresh"/></span>
                    <span className="location-icon" onClick={openLocations}><i className="fa-solid fa-location-dot"/></span>
                </div>
            </section>
        </>
    );
}

export default Header;