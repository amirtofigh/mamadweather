import React from 'react';


function Loader() {
    return (
        <div className="loader-wrapper">
            <span className="loader-container"/>
            <span className="loader"><i className="fa-solid fa-cloud-sun"/><span className="autoType"/>Mamad Weather</span>
        </div>
    );
}

export default Loader;