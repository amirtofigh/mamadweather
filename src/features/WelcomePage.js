import React from 'react';
import Logo from '../../src/img/Logo.png'

function WelcomePage() {
    return (
        <div className="welcome-container">
            <span className="welcome">
                Hi <i className="fa-solid fa-hand-peace"/>
                <br/>
                <span className="welcome-span"> Welcome to <img src={Logo} alt="Mamad Weather Logo"/> </span>
            </span>
            <span className="guide">
                <span className="guide-info">
                    No cities have been added yet!
                </span>
                <span className="guide-detail">
                    <span> " </span> To add a city, click the location button at the top of the page <span> " </span>
                </span>
            </span>
        </div>
    );
}

export default WelcomePage;