import React from 'react';
import {useSelector} from "react-redux";

function MainCityInfo() {
    const contentCreator = (data) => {
        let date = new Date(data.data.current.dt * 1000)
        const make2Digit = (input) => {
            if (input.toString().length === 1) {
                return '0' + input.toString()
            } else {
                return input
            }
        }
        return (
            <div className="info-container">
                <span className="city-country-name">
                    {data.city}, {data.country}
                </span>
                <span className="status-icon">
                    <img src={`https://openweathermap.org/img/wn/${data.data.current.weather[0].icon}@4x.png`} alt="status"/>
                </span>
                <span className="city-temperature">
                    {Math.round(data.data.current.temp)}
                </span>
                <span className="city-description">
                    {data.data.current.weather[0].description}
                </span>
                <span className="secondary-info">
                    <span className="feel-like">
                        Feel like: {Math.round(data.data.current.feels_like)} °C
                    </span>
                    <span className="wind-speed">
                        Wind speed: {data.data.current.wind_speed} Km/h
                    </span>
                    <span className="visibility">
                        Visibility:  {data.data.current.visibility} Km
                    </span>
                </span>
                <span className="update-time">
                    Updated in: &nbsp; {date.getFullYear()}, {make2Digit(date.getMonth())}, {make2Digit(date.getDate())} &nbsp; &nbsp; {make2Digit(date.getHours())} : {make2Digit(date.getMinutes())}
                </span>
            </div>
        )
    }

    let content;
    let mainCityData = useSelector(state => state.MainCity.mainCityData)
    if (mainCityData !== null) {
        content = contentCreator(mainCityData)
    }

    return (
        <section className="main-info">
            {content}
        </section>
    );
}

export default MainCityInfo;