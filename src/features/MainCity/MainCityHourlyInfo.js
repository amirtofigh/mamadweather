import React from 'react';
import {useSelector} from "react-redux";
import HourlyInfo from "./HourlyInfo";

function MainCityHourlyInfo() {
    let content;
    let mainCityData = useSelector(state => state.MainCity.mainCityData)

    if (mainCityData !== null) {
        content = mainCityData.data.hourly.map(data => <HourlyInfo data={data} key={data.dt}/>)
    } else {
        content = 'No City Selected'
    }

    return (
        <section className="hourly-info">
            <span className="section-title">
                Hourly Status
            </span>

            <div className="status-container">
                {content}
            </div>
        </section>
    );
}

export default MainCityHourlyInfo;