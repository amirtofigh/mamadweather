import React from 'react';
import {useSelector} from "react-redux";
import DailyInfo from "./DailyInfo";

function MainCityDailyInfo() {
    let content;
    let mainCityData = useSelector(state => state.MainCity.mainCityData)

    if (mainCityData !== null) {
        content = mainCityData.data.daily.map(data => <DailyInfo data={data} key={data.dt}/>)
    } else {
        content = 'No City Selected'
    }

    return (
        <section className="hourly-info">
            <span className="section-title">
                Daily Status
            </span>

            <div className="status-container">
                {content}
            </div>
        </section>
    );
}

export default MainCityDailyInfo;