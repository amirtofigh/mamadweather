import React from 'react';

function HourlyInfo(data) {
    let date = new Date(data.data.dt * 1000)

    return (
        <div className="status-option">
            <span className="status-title">
                <span className="date">
                    {date.getMonth()}, {date.getDate()}
                </span>
                <br/>
                {date.getHours()} : 00
            </span>
            <span className="status-icon">
                <img src={`https://openweathermap.org/img/wn/${data.data.weather[0].icon}@2x.png`} alt="status"/>
            </span>
            <span className="status-temp">
                {data.data.temp}
            </span>
            <span className="status-description">
                {data.data.weather[0].description}
            </span>
        </div>
    );
}

export default HourlyInfo;