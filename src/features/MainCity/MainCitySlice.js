import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import axiosInstance from "../../utils/axiosConfig"


export const APIKey = "87cf47cbabe6ea11f023b6516418cbca"

export const fetchMainCityData = createAsyncThunk('mainCity/fetchMainCityData', async (cityName) => {
    const latLonData = await axiosInstance.get(`/geo/1.0/direct?q=${cityName}&limit=1&appid=${APIKey}`)
        .then(res => res.data)

    const data = await axiosInstance.get(`/data/2.5/onecall?lat=${latLonData[0].lat}&lon=${latLonData[0].lon}&appid=${APIKey}&units=metric`)
        .then(res => res.data)
    return {
        data,
        city: latLonData[0].name,
        country: latLonData[0].country
    }
});


const initialState = {
    status: 'idle',
    mainCityData: null
}

const mainCitySlice = createSlice({
    name: 'mainCity',
    initialState,
    reducers: {},
    extraReducers: {
        [fetchMainCityData.pending]: (state) => {
            state.status = 'loading'
        },

        [fetchMainCityData.fulfilled]: (state, action) => {
            state.status = 'success';
            state.mainCityData = action.payload
        },

        [fetchMainCityData.rejected]: (state, action) => {
            state.status = 'error';
            window.alert(action.payload);
        }
    }
})

export default mainCitySlice.reducer