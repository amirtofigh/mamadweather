import React from 'react';

function DailyInfo(data) {
    let date = new Date(data.data.dt * 1000)

    return (
        <div className="status-option">
                <span className="status-title">
                    {date.getMonth()}, {date.getDate()}
                </span>
            <span className="status-icon">
                    <img src={`https://openweathermap.org/img/wn/${data.data.weather[0].icon}@2x.png`} alt="status"/>
                </span>
            <span className="temp">
                    <span className="status-temp min-temp">
                        {Math.round(data.data.temp.min)}
                    </span>
                    <span className="status-temp max-temp">
                        {Math.round(data.data.temp.max)}
                    </span>
                </span>
            <span className="status-description">
                    {data.data.weather[0].description}
                </span>
        </div>
    );
}

export default DailyInfo;