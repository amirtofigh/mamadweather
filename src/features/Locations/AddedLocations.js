import React from 'react';
import {useSelector} from "react-redux";
import AddedLocation from "./AddedLocation";


function AddedLocations() {
    let addedCitiesData = useSelector(state => state.Locations.addedCitiesData)
    const status = useSelector(state => state.Locations.status)

    let cities
    if ('loading' === status) {
        cities = <div className="loader" style={{marginTop: "2rem"}}>loading ...</div>
    } else if ('success' === status) {
        cities = addedCitiesData.map(city => <AddedLocation name={JSON.parse(city).cityName} temp={JSON.parse(city).temp}
            icon={JSON.parse(city).icon} key={JSON.parse(city).lat + JSON.parse(city).lon} lat={JSON.parse(city).lat} lon={JSON.parse(city).lon}/>);
    }

    if (!cities) {
        cities = <li><span className="locations-cities-name">No city added!</span></li>
    }

    return (
        <ul>
            {cities}
        </ul>
    );
}

export default AddedLocations;