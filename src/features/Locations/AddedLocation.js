import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {deleteAddedLocation, updateStateAfterDeleteLocation} from "./locationsSlice";
import {fetchMainCityData} from "../MainCity/MainCitySlice";


function AddedLocation({name, temp, icon, lon, lat}) {
    const dispatch = useDispatch()
    let mainCityData = useSelector(state => state.MainCity.mainCityData)

    const handelSelectLocation = (e) => {
        e.preventDefault()
        dispatch(fetchMainCityData(e.currentTarget.parentElement.parentElement.dataset.name))
    }

    const handelHoverTrash = (e) => {
        e.target.classList.remove("fa-trash")
        e.target.classList.add("fa-xmark")
    }

    const handelOnHoverTrash = (e) => {
        e.target.classList.add("fa-trash")
        e.target.classList.remove("fa-xmark")
    }

    const handelDeleteLocation = async (e) => {
        let addedCitiesData = JSON.parse(localStorage.getItem('addedCitiesData'))

        const currentLocation = {
            cityName: e.parentElement.parentElement.dataset.name,
            lat: e.parentElement.parentElement.dataset.lat,
            lon: e.parentElement.parentElement.dataset.lon
        }

        let index = 0
        let indexOfCurrent;
            addedCitiesData.forEach((data) => {
            if (data.lat.toString() === e.parentElement.parentElement.dataset.lat &&
                data.lon.toString() === e.parentElement.parentElement.dataset.lon) {
                indexOfCurrent = index
            } else if (index < addedCitiesData.length) {
                index+=1
            }
        })


        if (mainCityData !== null) {
            if(e.parentElement.parentElement.dataset.name === mainCityData.city) {
                addedCitiesData.splice(indexOfCurrent, 1)

                if (addedCitiesData.length > 0) {
                    dispatch(deleteAddedLocation({lat: currentLocation.lat, lon: currentLocation.lon ,name: currentLocation.cityName}))

                    localStorage.setItem('addedCitiesData', JSON.stringify(addedCitiesData))
                    dispatch(updateStateAfterDeleteLocation(addedCitiesData))
                    dispatch(fetchMainCityData(addedCitiesData[0].cityName))
                } else {
                    window.alert("The number of added cities is not enough to perform this action!")
                }
            } else {
                addedCitiesData.splice(indexOfCurrent, 1)

                await dispatch(deleteAddedLocation({lat: currentLocation.lat, lon: currentLocation.lon ,name: currentLocation.cityName}))

                localStorage.setItem('addedCitiesData', JSON.stringify(addedCitiesData))
                dispatch(updateStateAfterDeleteLocation(addedCitiesData))

                document.querySelectorAll("li").forEach(item => {
                    if (mainCityData.city === item.dataset.name) {
                        item.classList.add("selected")
                    } else {
                        item.classList.remove("selected")
                    }
                });

            }
        }
    }


    return (
        <li data-lon={lon} data-lat={lat} data-name={name}>
            <span className="delete-location"><i className="fa-solid fa-trash" onMouseOver={handelHoverTrash} onMouseLeave={handelOnHoverTrash} onClick={e => handelDeleteLocation(e.currentTarget)}/></span>
            <span className="locations-cities-name">
                <a href="/" onClick={e => handelSelectLocation(e)}>
                    {name}
                </a>
            </span>
            <span className="locations-cities-detail">
                <span className="temperature"> {Math.round(temp)} °C </span>
                <span className="status"> <img src={`https://openweathermap.org/img/wn/${icon}@4x.png`} alt="status"/> </span>
            </span>
        </li>
    );
}

export default AddedLocation;