import React, {useState} from 'react';
import {addLocation, fetchAddedLocations, getAddedCities} from "./locationsSlice";
import {useDispatch, useSelector} from "react-redux";
import {fetchMainCityData} from "../MainCity/MainCitySlice";


function AddLocation() {
    const dispatch = useDispatch();
    const status = useSelector(state => state.Locations.status)

    if (status === 'idle' && JSON.parse(localStorage.getItem('addedCitiesData'))) {
        dispatch(getAddedCities())
        JSON.parse(localStorage.getItem('addedCitiesData')).forEach(addedCity => {
            dispatch(fetchAddedLocations({lat: addedCity.lat, lon: addedCity.lon, name: addedCity.cityName}))
        })
        if (JSON.parse(localStorage.getItem('addedCitiesData'))) {
            dispatch(fetchMainCityData(JSON.parse(localStorage.getItem('addedCitiesData'))[0].cityName))
        }
    }

    const fetchAddedCities = (addedCities) => {
        addedCities.forEach(addedCity => {
            dispatch(fetchAddedLocations({lat: addedCity.lat, lon: addedCity.lon, name: addedCity.cityName}))
        })
    }

    const [cityName, setCityName] = useState('');

    const onChangeCityName = (e) => {
        setCityName(e.target.value);
    }

    const handelAddCity = async () => {
        if (cityName) {
            await dispatch(addLocation(cityName));

            let addedCities = JSON.parse(localStorage.getItem('addedCitiesData'))
            await fetchAddedCities(addedCities);

            dispatch(fetchMainCityData(cityName))

            setCityName('');
        }
    }

    const handleEnterPress = (e) => {
        if (cityName && e.key === "Enter") {
            e.preventDefault();

            handelAddCity().then(r => r)
        }
    }

    return (
        <span className="input">
            <input className="city-input" type="text" placeholder="Add City" value={cityName}
                   onChange={onChangeCityName} onKeyPress={handleEnterPress}/>
            <i className="fa-solid fa-plus" onClick={handelAddCity}/>
        </span>
    );
}

export default AddLocation;