import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import axiosInstance from "../../utils/axiosConfig"

export const APIKey = "87cf47cbabe6ea11f023b6516418cbca"



export const addLocation = createAsyncThunk('locations/addLocation', async (cityName) => {
    const latLonData = await axiosInstance.get(`/geo/1.0/direct?q=${cityName}&limit=1&appid=${APIKey}`)
        .then(res => res.data)

    const data = await axiosInstance.get(`/data/2.5/weather?lat=${latLonData[0].lat}&lon=${latLonData[0].lon}&appid=${APIKey}&units=metric`)
        .then(res => res.data)
    return {
        data,
        city: latLonData[0].name
    }
});

export const getAddedCities = createAsyncThunk('locations/getAddedCities', async () => {
    return localStorage.getItem('addedCitiesData');
});

export const fetchAddedLocations = createAsyncThunk('locations/fetchAddedLocations', async ({lat, lon, name}) => {
    const data = await axiosInstance.get(`/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${APIKey}&units=metric`)
        .then(res => res.data)
    return {
        name,
        data
    }
});

export const deleteAddedLocation = createAsyncThunk('locations/deleteAddedLocation', async ({lat, lon, name}) => {
    const data = await axiosInstance.get(`/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${APIKey}&units=metric`)
        .then(res => res.data)
    return {
        name,
        data
    }
});

const initialState = {
    status: 'idle',
    addedCities: [],
    addedCitiesData: []
}


const locationsSlice = createSlice({
    name: 'locations',
    initialState,
    reducers: {
        updateStateAfterDeleteLocation: (state, action) => {
            state.addedCities = action.payload
        }
    },
    extraReducers: {
        [addLocation.pending]: (state) => {
            state.status = 'loading'
        },

        [addLocation.fulfilled]: (state, action) => {
            state.status = 'success';
            const data = JSON.stringify({
                cityName: action.payload.city,
                lat: action.payload.data.coord.lat,
                lon: action.payload.data.coord.lon
            });
            if (!JSON.stringify(state.addedCities).includes(data)) {
                state.addedCities.push(JSON.parse(data))
                localStorage.setItem('addedCitiesData', JSON.stringify(state.addedCities))
            } else {
                window.alert("City already exist!")
            }
        },

        [addLocation.rejected]: (state, action) => {
            state.status = 'error';
            window.alert(action.payload);
        },

        [fetchAddedLocations.pending]: (state) => {
            state.status = 'loading'
        },

        [fetchAddedLocations.fulfilled]: (state, action) => {
            state.status = 'success';
            const data = JSON.stringify({
                cityName: action.payload.name,
                lat: action.payload.data.coord.lat,
                lon: action.payload.data.coord.lon,
                temp: action.payload.data.main.temp,
                icon: action.payload.data.weather[0].icon,
            });
            if (!state.addedCitiesData.includes(data)) {
                state.addedCitiesData.push(data)
            }
        },

        [fetchAddedLocations.rejected]: (state, action) => {
            state.status = 'error';
            window.alert(action.payload);
        },

        [deleteAddedLocation.pending]: (state) => {
            state.status = 'loading'
        },

        [deleteAddedLocation.fulfilled]: (state, action) => {
            state.status = 'success';
            let data = JSON.stringify({
                cityName: action.payload.name,
                lat: action.payload.data.coord.lat,
                lon: action.payload.data.coord.lon,
                temp: action.payload.data.main.temp,
                icon: action.payload.data.weather[0].icon,
            });
            let index = state.addedCitiesData.indexOf(data)
            if (index > -1) {
                state.addedCitiesData.splice(index, 1)
            }
        },

        [deleteAddedLocation.rejected]: (state, action) => {
            state.status = 'error';
            window.alert(action.payload);
        },

        [getAddedCities.pending]: (state) => {
            state.status = 'loading'
        },

        [getAddedCities.fulfilled]: (state, action) => {
            state.status = 'success';
            JSON.parse(action.payload).forEach(data => {
                if (!state.addedCities.includes(data)) {
                    state.addedCities.push(data);
                    localStorage.setItem('addedCitiesData', JSON.stringify(state.addedCities))
                } else {
                    window.alert("City already exist!")
                }
            });
        },

        [getAddedCities.rejected]: (state, action) => {
            state.status = 'error';
            window.alert(action.payload);
        }
    }
})

export const { updateStateAfterDeleteLocation } = locationsSlice.actions

export default locationsSlice.reducer