import React from 'react';
import AddLocation from "./AddLocation";
import AddedLocations from "./AddedLocations";


export const closeLocations = () => {
    if (document.querySelector(".locations").className === "locations locations-open") {
        document.querySelector(".locations").className = "locations"
    }
}

function Locations() {

    return (
        <section className="locations">
            <span className="close-locations" onClick={closeLocations}><i className="fa-solid fa-xmark"/></span>

            <AddLocation/>

            <div className="added-locations">
                <span className="added-location-title">
                    Added Locations
                </span>

                <AddedLocations/>
            </div>
        </section>
    );
}

export default Locations;