import React from 'react';
import Header from "./features/Header";
import Locations, {closeLocations} from "./features/Locations/Locations";
import * as BG from "./img/bgImageSelector";
import myLogo from "./img/AmirTofighLogo.png"
import {useSelector} from "react-redux";
import MainCityInfo from "./features/MainCity/MainCityInfo";
import MainCityHourlyInfo from "./features/MainCity/MainCityHourlyInfo";
import MainCityDailyInfo from "./features/MainCity/MainCityDailyInfo";
import Loader from "./features/Loader";
import WelcomePage from "./features/WelcomePage";



const handelScroll = () => {
    if (document.querySelector("main").scrollTop >= 80) {
        document.querySelector(".locations").classList.remove("locations-open")
        document.querySelector(".location-icon").style.display = "none"
    }
    else {
        document.querySelector(".location-icon").style.display = 'flex'
    }
}


function App() {
    let status = useSelector(state => state.MainCity.status)

    let weatherStatusBG;
    let mainCityData = useSelector(state => state.MainCity.mainCityData)
    if (mainCityData !== null) {
        weatherStatusBG = mainCityData.data.current.weather[0].icon
        document.querySelectorAll("li").forEach(item => {
            if (mainCityData.city === item.dataset.name) {
                item.classList.add("selected")
            } else {
                item.classList.remove("selected")
            }
        })
    } else {
        weatherStatusBG = 'default'
    }

    let content
    if (status === 'loading') {
        content = (
            <>
                <div className="background" style={{backgroundImage: `url(${BG[`BGdefault`]})`}}/>

                <main>
                    <Header/>

                    <Locations/>

                    <Loader onClick={closeLocations}/>
                </main>

                <div className="author">
                    Design & Developed by <a href="https://amirtofigh.com">Amir Tofigh</a> <img src={myLogo} alt="Amir Tofigh Logo"/>
                </div>
            </>
        )
    } else if (status === 'success') {
        content = (
            <>
                <div className="background" style={{backgroundImage: `url(${BG[`BG${weatherStatusBG}`]})`}}/>

                <main onScroll={handelScroll}>
                    <Header/>

                    <Locations/>
                    <section onClick={closeLocations}>
                        <MainCityInfo/>

                        <hr className="first-hr"/>

                        <MainCityHourlyInfo/>

                        <hr/>

                        <MainCityDailyInfo/>
                    </section>
                </main>

                <div className="author">
                    Design & Developed by <a href="https://amirtofigh.com">Amir Tofigh</a> <img src={myLogo} alt="Amir Tofigh Logo"/>
                </div>
            </>
        )
    } else if (status === 'error') {
        content = (
            <>
                <div className="background" style={{backgroundImage: `url(${BG[`BGdefault`]})`}}/>

                <main>
                    <Header/>

                    <Locations/>

                    <section className="error-section" onClick={closeLocations}> Something went wrong! <br/> Please try again ;) </section>
                </main>

                <div className="author">
                    Design & Developed by <a href="https://amirtofigh.com">Amir Tofigh</a> <img src={myLogo} alt="Amir Tofigh Logo"/>
                </div>
            </>
        )
    } else if (status === 'idle' || mainCityData === null) {
        content = (
            <>
                <div className="background" style={{backgroundImage: `url(${BG[`BGdefault`]})`}}/>

                <main>
                    <Header/>

                    <Locations/>

                    <WelcomePage onClick={closeLocations}/>
                </main>

                <div className="author">
                    Design & Developed by <a href="https://amirtofigh.com">Amir Tofigh</a> <img src={myLogo} alt="Amir Tofigh Logo"/>
                </div>
            </>
        )
    }

    return (
        <>
            {content}
        </>
    );
}

export default App;
